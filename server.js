const express = require('express');
const app = express();
const path = require('path');
const bodyparser = require('body-parser');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyparser.urlencoded({ extended: true }));

app.get("/", function (request, response) {
    response.render('index')
});

app.post("/userInformation", function (request, response) {
    response.render('userInformation', {
        fname: request.body['fname'],
        lname: request.body['lname'],
        email: request.body['email'],
        phone: request.body['phone'],
        dd: request.body['dd'],
        nn: request.body['nn'],
        yyyy: request.body['yyyy'],
        uname: request.body['uname'],
        pword: request.body['pword'],
    })
});

app.listen(3000)
console.log("Your Application is running on port 3000");